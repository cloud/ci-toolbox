FROM registry.gitlab.ics.muni.cz:443/cloud/container-registry/almalinux:8

LABEL Description="ci-toolbox"

ENV DEBIAN_FRONTEND=noninteractive \
    ANSIBLE_VERSION=2.9.21 \
    CINDER_CLIENT_VERSION=7.0.0 \
    HEAT_CLIENT_VERSION=1.18.0 \
    NEUTRON_CLIENT_VERSION=7.1.0 \
    NOVA_CLIENT_VERSION=17.0.0 \
    OS_CLIENT_VERSION=5.2.0 \
    OS_CLIENT_CONFIG_VERSION=2.1.0 \
    OPENSTACK_SDK=0.46.0 \
    PBR_VERSION=5.4.0 \
    PLACEMENT_CLIENT_VERSION=1.5.0


RUN yum -y install epel-release && yum update -y && yum install -y \
    wget bind-utils curl \
    rpm-build \
    openssh-clients crudini gcc gdisk git make \
    python3-devel python3-pip ncurses-devel zlib-devel\
    qemu-kvm qemu-img patch openssl-devel \
    unzip uuid xz-libs \
    python3-ruamel-yaml sshuttle iptables net-tools \
    && yum clean all


RUN pip3 --no-cache-dir install --upgrade virtualenv PyVmomi \
    && virtualenv --system-site-packages /opt/ansible

RUN pip3 --no-cache-dir install --upgrade pykickstart

ENV PATH /opt/ansible/bin:$PATH

RUN pip3 --no-cache-dir install --upgrade \
    "ansible == ${ANSIBLE_VERSION}" "cmd2 < 0.9.0" "openstacksdk == ${OPENSTACK_SDK}" PyMySQL pymongo dnspython pytz pyudev "pbr ~= ${PBR_VERSION}" "os-client-config ~= ${OS_CLIENT_CONFIG_VERSION}" \
    "python-novaclient ~= ${NOVA_CLIENT_VERSION}" python-keystoneclient "python-openstackclient == ${OS_CLIENT_VERSION}" "python-heatclient ~= ${HEAT_CLIENT_VERSION}" \
    "osc-placement ~= ${PLACEMENT_CLIENT_VERSION}" "python-neutronclient ~= ${NEUTRON_CLIENT_VERSION}" "python-cinderclient == ${CINDER_CLIENT_VERSION}" \
    python-monascaclient  python-octaviaclient python-magnumclient && mkdir -p /etc/ansible /usr/share/ansible \
    && echo 'localhost ansible_connection=local ansible_python_interpreter=/opt/ansible/bin/python' > /etc/ansible/hosts \
    && sed -i 's|  "identity_api_version": "2.0",|  "identity_api_version": "3",|' /opt/ansible/lib/python3.6/site-packages/os_client_config/defaults.json \
    && sed -i 's|  "volume_api_version": "2",|  "volume_api_version": "3",|' /opt/ansible/lib/python3.6/site-packages/os_client_config/defaults.json \
    && /opt/ansible/bin/openstack complete | tee /etc/bash_completion.d/osc.bash_completion > /dev/null \
    && /opt/ansible/bin/monasca complete | tee /etc/bash_completion.d/monasca.bash_completion > /dev/null

ENV ANSIBLE_LIBRARY /usr/share/ansible:$ANSIBLE_LIBRARY

ADD ./patches/ /patches/

RUN patch /opt/ansible/lib/python3.6/site-packages/openstackclient/network/v2/network_rbac.py /patches/network_rbac.py.patch \
    && patch /opt/ansible/lib/python3.6/site-packages/openstack/network/v2/rbac_policy.py /patches/rbac_policy.py.patch


# user is root